import React from "react"
import { Layout } from 'antd';
import Nav from "./Nav";
import Body from "./Body";
import Footer from "./CustomFooter";

const Main = () =>{

  return(
    <Layout className="layout">
      <Nav/>
      <Body/>
      <Footer/>
    </Layout>
  )
}

export default Main